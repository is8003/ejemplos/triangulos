# Triángulos

Usando asteriscos (`*`), imprime los siguientes cuatro triángulos:


```text
**********
*********
********
*******
******
*****
****
***
**
*

###########

*
**
***
****
*****
******
*******
********
*********
**********

###########

**********
 *********
  ********
   *******
    ******
     *****
      ****
       ***
        **
         *

###########

         *
        **
       ***
      ****
     *****
    ******
   *******
  ********
 *********
**********
```

## Refinamiento 1

1. Imprimo 10 líneas, cada línea con un asterisco menos desde 10 hasta 1.
1. Imprimo separador con numerales y líneas vacías.
1. Imprimo 10 líneas, cada línea con un asterisco mas desde 1 hasta 10.
1. Imprimo separador con numerales y líneas vacías.
1. Imprimo 10 líneas, cada línea con un espacio de más (desde 0 a 9) y un
   asterisco menos (desde 10 hasta 1).
1. Imprimo separador con numerales y líneas vacías.
1. Imprimo 10 líneas, cada línea con un espacio menos (desde 9 a 0) y un
   asterisco de más (desde 1 hasta 10).

## Refinamiento 2

1. Para  `linea` igual a `1`; hasta `línea` menor o igual a `10`; incremento
   `linea`:
   1. Para `aste` igual a `10`; hasta `aste` mayor o igual a `linea`;
      decremento `aste`:
      1. Imprimo asterisco
   1. Imprimo nueva línea
1. Imprimo separador con numerales y líneas vacías.
1. Para  `linea` igual a `1`; hasta `línea` menor o igual a `10`; incremento
   `linea`:
   1. Para `aste` igual a `1`; hasta `aste` menor o igual a `linea`;
      incremento `aste`:
      1. Imprimo asterisco
   1. Imprimo nueva línea
1. Imprimo separador con numerales y líneas vacías.
1. Para  `linea` desde `1`; hasta `línea` menor o igual a `10`; incremento
   `linea`:
   1. Para `espa` desde `2`; hasta `espa` menor o igual a `linea`; incremento
      `espa`:
      1. Imprimo espacio
   1. Para `aste` desde 10; hasta `aste` mayor o igual a `linea`; decremento
      `aste`:
      1. Imprimo asterisco
   1. Imprimo nueva línea
1. Imprimo separador con numerales y líneas vacías.
1. Para  `linea` desde `1`; hasta `línea` menor o igual a `10`; incremento
   `linea`:
   1. Para `espa` desde `9`; hasta `espa` mayor o igual a `linea`; decremento
      `espa`:
      1. Imprimo espacio
   1. Para `aste` desde 1; hasta `aste` menor o igual a `linea`; incremento
      `aste`:
      1. Imprimo asterisco
   1. Imprimo nueva línea

### Triángulo 1 - Diagrama de flujo

<div class="center">

```mermaid
graph TD
    inicio(("Inicio")) -->lineaIni["linea = 1"]
    lineaIni -->lineaLim{"linea <= 10"}
    lineaLim --"Falso"-->fin(("Fin"))
    lineaLim --"Verdadero"-->asteIni["aste = 10"]
    asteIni -->asteLim{"aste <= linea"}
    asteLim --"Verdadero"-->asteImp[/"Imprimo: *"/]
    asteImp -->asteAct["aste--"]
    asteAct -->asteLim
    asteLim --"Falso"-->lineaImp[/"Imprimo: \n"/]
    lineaImp -->lineaAct["linea++"]
    lineaAct -->lineaLim
```
</div>

### Triángulo 2 - Diagrama de flujo

<div class="center">

```mermaid
graph TD
    inicio(("Inicio")) -->lineaIni["linea = 1"]
    lineaIni -->lineaLim{"linea <= 10"}
    lineaLim --"Falso"-->fin(("Fin"))
    lineaLim --"Verdadero"-->asteIni["aste = 1"]
    asteIni -->asteLim{"aste <= linea"}
    asteLim --"Verdadero"-->asteImp[/"Imprimo: *"/]
    asteImp -->asteAct["aste++"]
    asteAct -->asteLim
    asteLim --"Falso"-->lineaImp[/"Imprimo: \n"/]
    lineaImp -->lineaAct["linea++"]
    lineaAct -->lineaLim
```
</div>

### Triángulo 3 - Diagrama de flujo

<div class="center">

```mermaid
graph TD
    inicio(("Inicio")) -->lineaIni["linea = 1"]
    lineaIni -->lineaLim{"linea <= 10"}
    lineaLim --"Falso"-->fin(("Fin"))
    lineaLim --"Verdadero"-->espaIni["espa = 2"]
    espaIni -->espaLim{"espa <= linea"}
    espaLim --"Verdadero"-->espaImp[/Imprimo: espacio/]
    espaImp -->espaAct["espa++"]
    espaAct -->espaLim
    espaLim --"Falso"-->asteIni["aste = 1"]
    asteIni -->asteLim{"aste <= linea"}
    asteLim --"Verdadero"-->asteImp[/"Imprimo: *"/]
    asteImp -->asteAct["aste--"]
    asteAct -->asteLim
    asteLim --"Falso"-->lineaImp[/"Imprimo: \n"/]
    lineaImp -->lineaAct["linea++"]
    lineaAct -->lineaLim
```
</div>

### Triángulo 4 - Diagrama de flujo

<div class="center">

```mermaid
graph TD
    inicio(("Inicio")) -->lineaIni["linea = 1"]
    lineaIni -->lineaLim{"linea <= 10"}
    lineaLim --"Falso"-->fin(("Fin"))
    lineaLim --"Verdadero"-->espaIni["espa = 9"]
    espaIni -->espaLim{"espa >= linea"}
    espaLim --"Verdadero"-->espaImp[/Imprimo: espacio/]
    espaImp -->espaAct["espa--"]
    espaAct -->espaLim
    espaLim --"Falso"-->asteIni["aste = 1"]
    asteIni -->asteLim{"aste <= linea"}
    asteLim --"Verdadero"-->asteImp[/"Imprimo: *"/]
    asteImp -->asteAct["aste++"]
    asteAct -->asteLim
    asteLim --"Falso"-->lineaImp[/"Imprimo: \n"/]
    lineaImp -->lineaAct["linea++"]
    lineaAct -->lineaLim
```
</div>
