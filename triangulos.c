#include <stdio.h>

int main(void){
	// Para  linea igual a 1; hasta línea menor o igual a 10; incremento
	// linea:
	for (unsigned int linea = 1; linea <= 10; linea++){
		// Para aste igual a 10; hasta aste menor o igual a linea;
		// decremento aste:
		for (unsigned int aste = 10; aste >= linea; aste--){
			// Imprimo asterisco
			printf("%s", "*");
		}
		// Imprimo nueva línea
		puts("");
	}

	// Imprimo separador con numerales y líneas vacías.
	printf("\n%s\n\n", "###########");

	// Para  linea igual a 1; hasta línea menor o igual a 10; incremento
	// linea:
	for (unsigned int linea = 1; linea <= 10; linea++){
		// Para aste igual a 1; hasta aste menor o igual a linea;
		// incremento aste:
		for (unsigned int aste = 1; aste <= linea; aste++){
			// Imprimo asterisco
			printf("%s", "*");
		}
		// Imprimo nueva línea
		puts("");
	}

	// Imprimo separador con numerales y líneas vacías.
	printf("\n%s\n\n", "###########");

	// Para  linea desde 1; hasta línea menor o igual a 10; incremento
	// linea:
	for (unsigned int linea = 1; linea <= 10; linea++){
		// Para espa desde 2; hasta espa menor o igual a linea;
		// incremento espa:
		for (unsigned int espa = 2; espa <= linea; espa++){
			// Imprimo espacio
			printf("%s", " ");
		}

		// Para aste desde 10; hasta aste mayor o igual a linea;
		// decremento aste:
		for (unsigned int aste = 10; aste >= linea; aste--){
			// Imprimo asterisco 
			printf("%s", "*");
		}
		// Imprimo nueva línea
		puts("");
	}

	// Imprimo separador con numerales y líneas vacías.
	printf("\n%s\n\n", "###########");

	// Para  linea desde 1; hasta línea menor o igual a 10; incremento
	// linea:
	for (unsigned int linea = 1; linea <= 10; linea++){
		// Para espa desde 9; hasta espa mayor o igual a linea;
		// decremento espa:
		for (unsigned int espa = 9; espa >= linea; espa--){
			// Imprimo espacio
			printf("%s", " ");
		}
		// Para aste desde 1; hasta aste menor o igual a linea;
		// incremento linea:
		for (unsigned int aste = 1; aste <= linea; aste++){
			// Imprimo asterisco
			printf("%s", "*");
		}
		// Imprimo nueva línea
		puts("");
	}
}
